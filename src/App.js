import React, { Component } from 'react';
import PropTypes from 'prop-types'

class App extends Component {
	constructor() {
		super()

		this.state = {
			text: ''
		}

		this.textRef = React.createRef()
	}

	// static defaultProps = {
	// 	userIsLoaded: false
	// }
	
	// static propTypes = {
	// 	userIsLoaded: PropTypes.bool,
	// 	user: PropTypes.shape({
	// 		_id: PropTypes.string,
	// 	}).isRequired
	// }

	run = () => {
		alert(2)
	}

	changeText = () => {
		this.setState({
			text: this.textRef.current.value
		})
		console.log(this.textRef)
	}

  render() {
    return (
      <React.Fragment>
      	<h1>Hello World</h1>
      	<button onClick={this.run}>Run</button>
      	<input type="text" ref={this.textRef} onChange={this.changeText}/>
      	<p>{ this.state.text }</p>
      </React.Fragment>
    );
  }
}

export default App;
